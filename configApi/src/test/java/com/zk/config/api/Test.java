package com.zk.config.api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.zk.config.api.constants.Constants;
import com.zk.config.api.util.ComUtil;
import com.zk.config.api.util.ZkCom;

import lombok.val;

public class Test {
	private String uString = "test";
	public static void main(String args[]){
//		Map<String, String> fileValue = new HashMap<>();
//		fileValue.put("a", "1");
//		
//		Map<String, String> fileValue2 = new HashMap<>();
//		fileValue2.putAll(fileValue);
//		fileValue2.put("a", "2");
//		
////		Map<String, String> fileValue3 = fileValue;
////		fileValue = fileValue2;
////		fileValue3.clear();
//		
//		for(String key:fileValue.keySet()) {
//			System.out.println(key + "=" + fileValue.get(key));	
//		}
//		
//		for(String key:fileValue2.keySet()) {
//			System.out.println(key + "=" + fileValue2.get(key));	
//		}
//		
////		for(String key:fileValue3.keySet()) {
////			System.out.println(key + "=" + fileValue3.get(key));
////		}
//		
//		new Test().exchange();
//		String zkPath = "/root/conf/config.text";
//		int i = -1;
//		
//		while((i = zkPath.lastIndexOf("/")) != -1) {
//			zkPath = zkPath.substring(0, i);
//			System.out.println(zkPath + ", " +i);
//		}
//		Map<String, String> map = new TreeMap();
//		map.put("/root", "1");
//		map.put("/root/conf", "1");
//		map.put("/root/conf/config.text", "1");
//		map.put("/root/aonf", null);
//		map.put("/root/aonf/a.best", "1");
//		map.put("/root/aonf/b.aest", "1");
//		for(String k:map.keySet()) {
//			System.out.println(k + ", " + map.get(k));
//		}
//		System.out.println(System.getProperty("java.class.path"));
//		String path = "/root/aonf/b.aest";
//		System.out.println(Constants.mapingDir);
//		System.out.println(new Date(new File(Constants.mapingDir).lastModified()));
//		System.out.println(ZkCom.getZkPath(null,"/ba", "c"));
		Set<String> a = new HashSet<>();
		a.add(null);
		a.add("a");
		a.add(null);
		if(a.size() > 0) {
			for(String b:a) {
				System.out.println(b);
			}
		}
//		System.out.println(path.substring(path.lastIndexOf("/")+1));
//		File file2 = new File("c:/1/1.txt");
//		ComUtil.writeFile(file2, null);
	}
	
	public void exchange(){
		TimeThread time = new TimeThread(500);	
		time.start();
	}
	
	class TimeThread extends Thread {
		private int time;
		private boolean runflag = true;
		public TimeThread(int time) {
			this.time = time;
		}
		public void run() {
			while(runflag) {
				try {
					this.sleep(time);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(uString);
				close();
			}
		}
		
		public void close(){
			runflag = false;
			try {
				this.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
