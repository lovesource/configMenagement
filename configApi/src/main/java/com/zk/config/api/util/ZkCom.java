package com.zk.config.api.util;

import org.apache.commons.lang3.StringUtils;

public class ZkCom {
	public static String getZkPath(String zkPath){
		if(zkPath == null || zkPath.length() == 0 || "/".equals(zkPath)) {
			return "/";
		}
		if (!zkPath.startsWith("/")) {
			zkPath = "/" + zkPath;
		}
		if(zkPath.endsWith("/")) {
			zkPath = zkPath.substring(0, zkPath.lastIndexOf("/"));
		}
		zkPath = zkPath.replaceAll("/+", "/");
		return zkPath;
	}
	
	public static String getZkEndPath(String zkPath){
		if(zkPath == null || zkPath.length() == 0 || "/".equals(zkPath)) {
			return "/";
		}
		if (!zkPath.startsWith("/")) {
			zkPath = "/" + zkPath;
		}
		if (!zkPath.endsWith("/")) {
			zkPath = zkPath + "/";
		}
		zkPath = zkPath.replaceAll("/+", "/");
		return zkPath;
	}
	
	/**
	 * 判断路径是否包含
	 * @param srcPath
	 * @param shortPath
	 * @return
	 */
	public static boolean checkPath(String checkPath, String path) {
		boolean result = false;
		if (StringUtils.isNotEmpty(checkPath) && StringUtils.isNotEmpty(path)) {
			if (!checkPath.endsWith("/")) {
				checkPath +="/";
			}
			if (!path.endsWith("/")) {
				path +="/";
			}
			if("/".equals(path) || "/".equals(checkPath)) {
				return false;
			}
			if (checkPath.indexOf(path) == 0) {
				return true;
			}
		}
		
		return result;
	}
	
	public static String getZkPath(String ...path){
		String pathStr = "";
		if (path != null && path.length > 0) {
			for(String p:path) {
				pathStr += ZkCom.getZkPath(p);
			}
			pathStr = pathStr.replaceAll("/+", "/");
		}
		return pathStr;
	}
}
