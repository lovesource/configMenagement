/**
 * 
 */
package com.zk.config.web.model;

import lombok.Data;

@Data
public class Param {
    private String path = "";
    private String key = "";
    private String wt = "json";
    private String userName = "";
    private String password = "";
}
