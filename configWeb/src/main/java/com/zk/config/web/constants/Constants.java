package com.zk.config.web.constants;

import java.nio.charset.Charset;

public class Constants {

	public static final String CX_STR = "cxnstr";
	
	public static Charset defualtCharset = Charset.forName("UTF-8");
	
	public static String classPathDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	
	public static String uploadDir = classPathDir+"../../uploadDir";
	
	public static String downloadDir = classPathDir+"../../downloadDir";
	
	public static long maxWaitTime = 5000;
	
	public static long waitStepTime = 50;
}
