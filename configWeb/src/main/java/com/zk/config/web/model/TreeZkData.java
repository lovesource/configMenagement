package com.zk.config.web.model;

import java.util.List;
import lombok.Data;

@Data
public class TreeZkData {
    private String name;
	private String data;
	private List<TreeZkData> children; // 子节点
}
