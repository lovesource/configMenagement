package com.zk.config.web.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class Response implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6124886288083051210L;
	
	private int state = 0; // 0:success,非零:fail
	private String msg = "";
	private Object data;
}
