package com.zk.config.web.model;

import java.util.Date;
import lombok.Data;

@Data
public class FileInfo {
	private String name;
	private long size;
	private Date time;
	private long timeLong;
}
