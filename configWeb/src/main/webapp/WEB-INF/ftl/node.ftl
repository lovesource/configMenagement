<!DOCTYPE html>
<html>
	<head>
		<title>Zookeeper-Web</title>
		<link href="${host}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="${host}/css/zk-web.css" rel="stylesheet" type="text/css">
		<link href="${host}/jstree/themes/default/style.min.css" rel="stylesheet" type="text/css"/>
		<script src="${host}/js/jquery-2.1.4.min.js" type="text/javascript"></script>
		<script src="${host}/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${host}/jstree/jstree.min.js" type="text/javascript"></script>
		<script src="${host}/js/node.js" type="text/javascript"></script>
		<style type="text/css">
		#tree .folder { background:url('${host}/jstree/file_sprite.png') right bottom no-repeat; width:18px;height:18px;}
		#tree .file { background:url('${host}/jstree/file_sprite.png') 0px 0px no-repeat; width:18px;height:18px;}
		</style>
	</head>
	<body>
		
<div class="container-fluid">
		<#if baseMsg?? && baseMsg.errMsg?? && baseMsg.errMsg?size != 0>
		<div class="alert alert-error" style="background:#FFB6C1">
			 <button type="button" class="close" data-dismiss="alert">×</button>
			 <#list baseMsg.errMsg as msg>
			<strong style="color:#CD5C5C">${msg}</strong>
			</#list>
		</div>
		</#if>
		<div class="alert alert-info">
			 <button type="button" class="close" data-dismiss="alert">×</button>
			<h4>
				已选择路径:
			</h4> <strong><a href="${host}/read/addr?cxnstr=${cxnstr!''}">${cxnstr!''}</a></strong><strong id="pathAppend">${zkpath!""}</strong>
		</div>
	<div class="row-fluid clearfix">
		<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="margin:0;padding:0;">
			<div class="navbar-header" style="margin-left:2%;">
				<span class="navbar-brand" style="padding-top:5%;"><a href="${host}/" class="btn btn-info">回到首页</a></span>
			</div>
			<#if isUseCache() == 1>
				<div id="openCacheDiv" class="navbar-header" style="margin-left:2%;display:none;">
					<span class="navbar-brand" style="padding-top:5%;"><a href="#" onclick="openCache()" class="btn btn-info">缓存加速</a></span>
				</div>
				<div id="closeCacheDiv" class="navbar-header" style="margin-left:2%;">
					<span class="navbar-brand" style="padding-top:5%;"><a href="#" onclick="closeCache()" class="btn btn-info">关闭缓存</a></span>
				</div>
			<#else>
				<div id="openCacheDiv" class="navbar-header" style="margin-left:2%;">
					<span class="navbar-brand" style="padding-top:5%;"><a href="#" onclick="openCache()" class="btn btn-info">缓存加速</a></span>
				</div>
				<div id="closeCacheDiv" class="navbar-header" style="margin-left:2%;display:none;">
					<span class="navbar-brand" style="padding-top:5%;"><a href="#" onclick="closeCache()" class="btn btn-info">关闭缓存</a></span>
				</div>
			</#if>
			<#assign loginStatus=isLogin() />
			<#if loginStatus != 0>
			<div class="navbar-header" style="margin-left:15%;">
				 <span class="navbar-brand" style="padding-top:15%;">管理工具</span>
			</div>
			<input type="hidden" id="loginStatus" value="${loginStatus}">
			<#else>
			<input type="hidden" id="loginStatus" value="0">
			</#if>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" align="center" valign="top">
				<#if loginStatus != 0>
				<ul class="nav navbar-nav">
					<#if loginStatus == 1>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#createModal" href="#createModal" class="btn btn-primary">创建</a></span>
					</li>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#editModal" href="#editModal" class="btn btn-primary">编辑</a></span>
					</li>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#deleteModal" href="#deleteModal" class="btn btn-danger">删除</a></span>
					</li>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#rmrModal" href="#rmrModal" class="btn btn-danger">级联删除</a></span>
					</li>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#uploadModal" href="#uploadModal" class="btn btn-primary">上传</a></span>
					</li>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#downloadModal" href="#downloadModal" class="btn btn-primary">下载</a></span>
					</li>
					</#if>
					<#if loginStatus != 0>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#backupModal" href="#backupModal" class="btn btn-primary">备份</a></span>
					</li>
					</#if>
					<#if loginStatus == 1>
					<li>
						<span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#recoverModal" href="#recoverModal" class="btn btn-primary">还原</a></span>
					</li>
					</#if>
				</ul>
				</#if>
				<ul class="nav nav-pills navbar-right" style="margin-right: 5%;">
					<li>
						 <span class="navbar-brand" style="padding-top:15%;"><a><#if loginStatus != 0>${zk_user!""}<#else>游客</#if></a></span>
					</li>
					<li class="active">
						 <#if loginStatus != 0>
						 <span class="navbar-brand" style="padding-top:5%;"><a href="${host}/logout" class="btn btn-success">注销</a></span>
						 <#else>
						 <span class="navbar-brand" style="padding-top:5%;"><a data-toggle="modal" data-target="#loginModal" href="#loginModal" class="btn btn-warning">登录</a></span>
						 </#if>
					</li>
				</ul>
			</div>
		</nav>
		<div class="col-md-6 column">
			<div class="float">
		        <input class="form-control" required id="searchNode" placeholder="输入节点名称检索" type="text" />
				<h3>节点</h3>
				<div id="container">
		            <div id="tree"></div>
				</div>
			</div>	
		</div>
		<input id="zkpath" type="hidden" value="${zkpath!''}" />
		<div class="col-md-6 column" id="rightContent" style="overflow:hidden;">
		</div>
	</div>
</div>

<div class="modal fade" id="recoverModal">
  <div class="modal-dialog" style="width:50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">还原备份到当前根目录或删除备份</h4>
      </div>
      <form action="${host}/op/recover" method="POST" class="form-horizontal">
      <div class="modal-body" style="overflow:auto;height:500px;">
      	<#if fileList?? && fileList?size != 0>
    	<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>选择</th>
					<th>备份文件</th>
					<th>大小</th>
					<th>备份时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
			<#assign colors = ["success","error","warning","info"]>
			 <#list fileList as file>
				<tr id="bfiletr${file_index}" class="${colors[file_index%4]}">
					<td>
						<div class="radio">
						  <#if file_index == 0>
							 <label><input type="radio" name="fileName" id="optionsRadios${file_index}" value="${file.name}" checked/> 文件${file_index+1}</label>
						  <#else>
						     <label><input type="radio" name="fileName" id="optionsRadios${file_index}" value="${file.name}"/> 文件${file_index+1}</label>
						  </#if>
						</div>
					</td>
					<td>
						${file.name}
					</td>
					<td>
						${file.size/1024}KB
					</td>
					<td>
						${file.time?string('yyyy-MM-dd HH:mm')}
					</td>
					<td>
						<button class="btn btn-mini btn-danger" type="button" onclick="deleteBackupFile(${file_index},'${file.name}')">删除备份</button>
					</td>
				</tr>
				</#list>
			</tbody>
		</table>
		<#else>
			<div class="alert alert-info"><strong>你还没有进行过备份哦！</strong></div>
		</#if>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <#if fileList?? && fileList?size != 0>
        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">还原</button>
        </#if>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="uploadModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">上传文件</h4>
      </div>
      <form action="${host}/op/upload" method="POST" enctype="multipart/form-data" class="form-horizontal">
      <div class="modal-body">
    	<div class="alert alert-info">文件上传到这个节点下: <strong id="uploadpath">${zkpath!""}</strong></div>
    	<div class="form-group" style="margin-right: 5%;">
    		<label for="inputName" class="col-lg-2 control-label">文件:</label>
	    	<div class="col-lg-10">
	    		<input type="file" required name="file" id="inputName"/></td>
			</div>
			<div class="col-lg-10">
				<font color="blue">* 如果是目录请上传rar或zip压缩格式的文件！</font>
			</div>
	    </div>
		<input class="span8" name="path" id="uploadpath2" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">上传</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="downloadModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">下载节点数据</h4>
      </div>
      <form action="${host}/op/download" method="POST" class="form-horizontal">
      <div class="modal-body">
    	<div class="alert alert-info">确认下载节点: <strong id="downloadpath">${zkpath!""}</strong></div>
		<input class="span8" name="path" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" id="del-btn" class="btn btn-primary" onclick="this.disabled=true;window.document.body.style.cursor='wait';this.form.submit();$('#downloadModal').modal('hide');window.document.body.style.cursor='default';this.disabled=false;">下载</button>
      </div>
      </form>
    </div>
  </div>
</div>
		
<div class="modal fade" id="createModal">
  <div class="modal-dialog" style="width:50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">创建一个子节点</h4>
      </div>
      <form action="${host}/op/create" method="POST" class="form-horizontal">
      <div class="modal-body" style="height:500px;">
    	<div class="alert alert-info">从这个节点创建子节点: <strong id="createpath">${zkpath!""}</strong></div>
    	<div class="form-group">
	      <label for="inputName" class="col-lg-2 control-label">节点名称:</label>
	      <div class="col-lg-10">
	        <input class="form-control" required name="name" id="inputName" placeholder="Name of new node" type="text" />
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="textAreaData" class="col-lg-2 control-label">数据:</label>
	      <div class="col-lg-10">
	        <textarea class="form-control" rows="16" name="data" id="textAreaData" placeholder="Data of new node"></textarea>
	      </div>
	    </div>
		<input class="span8" name="parent" id="createpath2" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">创建</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal">
  <div class="modal-dialog" style="width:50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">编辑节点数据</h4>
      </div>
      <form action="${host}/op/edit" method="POST" class="form-horizontal">
      <div class="modal-body" style="height:500px;">
    	<div class="alert alert-info">编辑节点: <strong id="editpath">${zkpath!""}</strong></div>
	    <div class="form-group">
	      <label for="textAreaData2" class="col-lg-2 control-label">数据:</label>
	      <div class="col-lg-10">
	        <textarea class="form-control" rows="18" name="data" id="textAreaData2" placeholder="Data of new node"></textarea>
	      </div>
	    </div>
		<input class="span8" name="path" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">删除这个节点</h4>
      </div>
      <form action="${host}/op/delete" method="POST" class="form-horizontal">
      <div class="modal-body">
    	<div class="alert alert-info">确认删除节点: <strong id="deletepath">${zkpath!""}</strong></div>
		<input class="span8" name="path" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" id="del-btn" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">删除</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="backupModal">
  <div class="modal-dialog" style="width:50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">备份这个节点</h4>
      </div>
      <form action="${host}/op/backup" method="POST" class="form-horizontal">
      <div class="modal-body">
    	<div class="alert alert-info">确认备份节点: <strong id="backuppath">${zkpath!""}</strong></div>
		<input class="span8" name="path" type="hidden" value="${zkpath!''}" />
		<div class="form-group">
	      <label for="inputFileName" class="col-lg-2 control-label">备份文件的名称:</label>
	      <div class="col-lg-10">
	        <input class="form-control" required name="fileName" id="inputFileName" placeholder="FileName for backup" type="text"/>
	        <font color="blue">* 不填则默认为节点名加时间戳作为文件名！</font>
	      </div>
	    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" id="backup-btn"  class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">备份</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="rmrModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">删除这个节点及子节点</h4>
      </div>
      <form action="${host}/op/rmrdel" method="POST" class="form-horizontal">
      <div class="modal-body">
    	<div class="alert alert-danger">
    		<h4>Danger!!</h4>
    		确定级联删除: <strong id="rmrpath">${zkpath!""}</strong>和所有的子节点???
    	</div>
		<input class="span8" name="path" type="hidden" value="${zkpath!''}" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" id="del-btn" class="btn btn-primary" onclick="this.disabled=true;this.form.submit()">级联删除</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">登录</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block" action="${host}/login" method="POST">
            <div class="form-group">
              <input required name="username" type="text" class="form-control input-lg" placeholder="用户名">
            </div>
            <div class="form-group">
              <input required name="password" type="password" class="form-control input-lg" placeholder="密码">
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block" onclick="this.disabled=true;this.form.submit()">登录</button>
            </div>
          </form>
      </div>
      <div class="modal-footer-login modal-footer">
          <div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		  </div>
      </div>
  </div>
  </div>
</div>
<script type="text/javascript">
		init();
		function init() {
			var zkpath = '${zkpath!''}';
			if(zkpath) {
				loadRight(zkpath);
			}
		}
</script>
	</body>
</html>